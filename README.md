# WEBFORM_TRANSLATION_PLUS

## INTRODUCTION

This module is to fix a few things in webform_translation without having to drastically alter the parent module and permit these options to be ignored if need be. 

*** Should the maintainer of the module wish these patched in, I would be willing to discuss what would be needed. ***

When translating forms, placeholder text which is a relatively new option, is missing. This module adds the placeholder field to the translatable values. Also, since some of us use node_export module to manage our webform deployments, this adds those translated fields into the node_export output and import.

## INSTALLATION
1. Copy webform_translation_plus folder to modules directory (usually sites/all/modules).
1. That's it, work with webform components like ususal. If you have webform_translation active you'll see the placeholder option available in qualified components.


## CONFIGURATION
* not applicable


## USAGE

1. Check out the WEBFORM_TRANSLATION documentation.
